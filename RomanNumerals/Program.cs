﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumerals
{
    class RomanNumber
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<int> integerVariant = new List<int>();
            integerVariant.Add(1);
            integerVariant.Add(5);
            integerVariant.Add(10);
            integerVariant.Add(50);
            integerVariant.Add(100);
            integerVariant.Add(500);
            integerVariant.Add(1000);

            foreach (var index in integerVariant.ToList())
            {
                var lastItem = integerVariant[integerVariant.Count - 1];
                var convIntoString = lastItem.ToString();
                int nextValue = 0;
                if (convIntoString.StartsWith("1"))
                {
                    nextValue = lastItem * 5;
                    integerVariant.Add(nextValue);
                }
                else
                {
                    nextValue = lastItem * 2;
                    integerVariant.Add(nextValue);
                }
                if (integerVariant.Count() == 10)
                    break;
            }
        }

        static void ConvertRomanNum()
        {
            
        }
    }
}
